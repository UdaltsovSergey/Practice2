package ua.nure.udaltsov.Practice2;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Objects;

public class MyListImpl implements MyList, ListIterable {

    private Object[] array;

    private int nextElementIndex;

    MyListImpl() {
        this.array = new Object[10];
        this.nextElementIndex = 0;
    }

    private void checkArrayCapacity() {

        if (nextElementIndex == array.length) {

            Object[] tempArray = new Object[array.length * 2];

            for (int i = 0; i < array.length; i++) {

                tempArray[i] = array[i];

            }
            array = tempArray;
        }

    }

    @Override
    public void add(Object e) {

        if (Objects.nonNull(e)) {

            checkArrayCapacity();

            array[nextElementIndex] = e;

            nextElementIndex++;
        }

    }

    @Override
    public String toString() {

		return Arrays.toString(Arrays.copyOf(array, nextElementIndex));
    }


    @Override
    public void clear() {

        array = new Object[array.length];

        nextElementIndex = 0;
    }

    @Override
    public boolean remove(Object o) {

        for (int i = 0; i < array.length; i++) {


            if (array[i].equals(o)) {
                Object[] tempArray = new Object[array.length - 1];

                for (int j = 0; j < i; j++) {
                    tempArray[j] = array[j];
                }

                for (int j = i; j < tempArray.length; j++) {
                    tempArray[j] = array[j + 1];
                }
                array = tempArray;
                nextElementIndex--;
                return true;
            }

        }
        return false;
    }

    @Override
    public Object[] toArray() {

        return Arrays.copyOf(array, nextElementIndex);
    }

    @Override
    public int size() {
        return nextElementIndex;
    }

    @Override
    public boolean contains(Object o) {

        if (Objects.isNull(o)) {
            return false;
        }

        int index = 0;

        while (Objects.nonNull(array[index])) {

            if (array[index].equals(o)) {
                return true;
            }
            index++;
        }
        return false;
    }

    @Override
    public boolean containsAll(MyList c) {

        if (Objects.isNull(c)) {
            return false;
        }

        int count = 0;

        for (Object el : c.toArray()) {

            int index = 0;

            while (Objects.nonNull(array[index])) {
                if (array[index].equals(el)) {

                    count++;

                    break;
                }
                index++;
            }
        }
        return count == c.toArray().length;
    }

    @Override
    public Iterator<Object> iterator() {

        return new IteratorImpl(this);
    }

    void set(Object o, int index) {

        array[index] = o;

    }

    void removeByIndex(int index) {
        Object[] tempArray = new Object[array.length - 1];

        for (int j = 0; j < index; j++) {
            tempArray[j] = array[j];
        }

        for (int j = index; j < tempArray.length; j++) {
            tempArray[j] = array[j + 1];
        }
        array = tempArray;
		nextElementIndex--;
    }

    @Override
    public ListIterator listIterator() {
        return new ListIteratorImpl(this);
    }
}
