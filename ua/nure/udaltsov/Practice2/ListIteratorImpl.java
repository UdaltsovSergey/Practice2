package ua.nure.udaltsov.Practice2;

import java.util.NoSuchElementException;

public class ListIteratorImpl extends IteratorImpl implements ListIterator {

    private MyListImpl list;

    private boolean setMarker;

    ListIteratorImpl(MyListImpl list) {

        super(list);
        this.list = list;
        this.setMarker = true;
    }


    @Override
    public boolean hasPrevious() {
        return lastReturnedElement > 0;
    }

    @Override
    public Object previous() {

        if (lastReturnedElement == 0) {
            throw new NoSuchElementException();
        }

        Object[] array = list.toArray();

        lastReturnedElement = nextElementIndex - 1;

        return array[--nextElementIndex];
    }

    @Override
    public void set(Object e) {

        if (!checkCondition()) {
            throw new IllegalStateException();
        }

        list.set(e, nextElementIndex - 1);
        lastReturnedElement = 0;
    }
}
