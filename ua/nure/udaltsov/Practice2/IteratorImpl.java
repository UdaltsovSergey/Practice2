package ua.nure.udaltsov.Practice2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class IteratorImpl implements Iterator<Object> {


    int nextElementIndex;

    int lastReturnedElement;

    private MyListImpl list;

    IteratorImpl(MyListImpl list) {
        this.list = list;
        this.nextElementIndex = 0;
        this.lastReturnedElement = 0;
    }

    public boolean hasNext() {

        Object[] array = list.toArray();

        return nextElementIndex != array.length;
    }

    public Object next() {

        Object[] array = list.toArray();

        if (nextElementIndex == array.length){
            throw new NoSuchElementException();
        }

        lastReturnedElement = nextElementIndex + 1;

        return array[nextElementIndex++];
    }

    public void remove() {

        if (!checkCondition()) {
            throw new IllegalStateException();
        }

        if (nextElementIndex == 0) {
            throw new NoSuchElementException();
        }

        int indexToRemove = nextElementIndex - 1;

        list.removeByIndex(indexToRemove);

        nextElementIndex--;

    }

    boolean checkCondition() {
        return lastReturnedElement == nextElementIndex;
    }

}
